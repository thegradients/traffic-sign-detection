import os, sys
import numpy as np
"""
save train data and corresponding lables and test data
"""
trainingset = []
traininglabel = []
classes = np.arange(0, 43)
for Class_id in classes:
	path = "hog_features/HOG_training/" + str(Class_id)
	print path
	dirs = os.listdir(path)
	for file in dirs:
		with open(str(path)+'/'+str(file)) as f:
		    content = f.readlines()
		content = [x.strip() for x in content]
		trainingset.append(content)
		traininglabel.append(Class_id)
np.save('training_set.npy', trainingset)
np.save('training_labels.npy', traininglabel)	