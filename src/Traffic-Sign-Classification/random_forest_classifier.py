import numpy as np
from sklearn.metrics import precision_recall_fscore_support
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import *
X = np.load('training_set.npy')
Y = np.load('training_labels.npy')
X1 = np.load('test_set.npy')
"""
parameters can be changed to check the performance, mainly n_estimators- this means no. of forest, f will predict accuracy
"""
classifier = RandomForestClassifier(n_estimators=10, criterion='gini', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto', max_leaf_nodes=None, min_impurity_split=1e-07, bootstrap=True, oob_score=False, n_jobs=1, random_state=None, verbose=0, warm_start=False, class_weight=None)
train_model = classifier.fit(X, Y)
test_model = train_model.predict(X1)
p,r,f,s = precision_recall_fscore_support(Y, test_model, average='weighted')