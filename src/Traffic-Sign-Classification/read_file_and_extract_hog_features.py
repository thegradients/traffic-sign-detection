import cv2
import numpy as np
import csv



def extract_hog_features(X, feature):
	"""
	extract features on smaller image
	"""
	# smaller size of image for hog to be performed
	small_size = (32, 32)
	X = [cv2.resize(x, small_size) for x in X]

	# computing hog, can vary size to check performance
	block_size = (small_size[0] / 2, small_size[1] / 2)
	block_stride = (small_size[0] / 4, small_size[1] / 4)
	cell_size = block_stride
	num_bins = 9
	hog = cv2.HOGDescriptor(small_size, block_size, block_stride, cell_size, num_bins)
	X = [hog.compute(x) for x in X]
    X = [x.flatten() for x in X]
	return X


def get_images_from_directory(path_of_image, feature_required, region_of_interest, test_split, seed):
    """
    read images from directory and return hog features of image corresponding to each class
    """
    
	#no of classes
	no_of_classes = np.arange(0, 43)
	"""
	X store images and labels corresponding label
	"""
	X = []
	labels = []
	for c in xrange(len(no_of_classes)):
		full_path_of_image_file = path_of_image + '/' + format(no_of_classes[c], '05d') + '/'
		csv_file = open(full_path_of_image_file + 'GT-' + format(no_of_classes[c], '05d') + '.csv')
		# read from csv file
		csv_file_reader = csv.reader(csv_file, delimiter=';')
		csv_file_reader.next()
		for row in csv_file_reader:
			resultant_image = cv2.imread(full_path_of_image_file + row[0])
			# region of interest
			if region_of_interest:
				resultant_image = resultant_image[np.int(row[4]):np.int(row[6]), np.int(row[3]):np.int(row[5]), :]

			X.append(resultant_image)
			labels.append(c)
		csv_file.close()

	X = extract_hog_features(X, feature_required)

	np.random.seed(seed)
	np.random.shuffle(X)
	np.random.shuffle(labels)

	X_train = X[:int(len(X)*(1-test_split))]
	y_train = labels[:int(len(X)*(1-test_split))]

	X_test = X[int(len(X)*(1-test_split)):]
	y_test = labels[int(len(X)*(1-test_split)):]

	return (X_train, y_train), (X_test, y_test)